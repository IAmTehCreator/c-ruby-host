# C Ruby host
This repository contains a simple C application which can host plugins written in Ruby.

## Building
This project uses [Apache Ant](http://ant.apache.org/) as a build system. Build configuration is done within the `build.properties` and `local.build.properties` files. To build the project you must specify the `libs.all` property which should contain the paths to the Ruby C API separated by a colon. On Mac OS X this should look similar to;

```
ruby.include = /Users/SMilne/.rvm/rubies/ruby-2.2.2/include/ruby-2.2.0
ruby.include.bin = /Users/SMilne/.rvm/rubies/ruby-2.2.2/include/ruby-2.2.0/x86_64-darwin14

libs.all = ${ruby.include};${ruby.include.bin}
```

Once the libraries have been referenced in a build properties file you can then compile and run the application. Below is a list of the ant targets that this project supports;

* `ant init` - Extract and setup dependencies
* `ant clean` - Delete build files
* `ant compile` - Compiles the application under `bin/main.o`
* `ant run` - Compiles and run the application
* `ant test` - Runs the C based unit tests

### Libraries
This project makes use of some [C libraries](https://bitbucket.org/IAmTehCreator/c-library) which are included in pre-compiled form within the `lib/` directory. These must be extracted and setup using the `ant init` command before you can compile the application.
