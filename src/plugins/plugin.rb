Api.log "Hello from the plugin API (version #{Api::VERSION})!";

Api.on 'thing' do |i|
	puts "Plugin callback called with #{i}!"
end

Api.on 'log' do |message|
	puts "Ruby: #{message}"
end
