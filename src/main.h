#ifndef TEST_HEADER
#define TEST_HEADER

/* Macros *********************************************************************/
#define SCRIPT_NAME "ruby_api"
#define API_MODULE "Api"
#define API_VERSION "0.0.0"
#define MESSAGE "Hello World from C!\n"

/* Global Function Prototypes *************************************************/
int main();

#endif
