#include <ruby.h>
#include <stdio.h>
#include "main.h"
#include "ruby/helper.h"
#include "ruby/api.h"

int main() {
	printf(MESSAGE);

	ruby_start(SCRIPT_NAME);
	ruby_define_api(API_MODULE, API_VERSION);
	ruby_run_plugins("./src/plugins");

	ruby_api_fire("thing", "stuff");
	ruby_api_fire("log", "message");

	return ruby_end(0);
}
