#ifndef RUBY_HELPER_HEADER
#define RUBY_HELPER_HEADER

/* Macros *********************************************************************/
#define RUBY_SUFFIX ".rb"

/* Global Function Prototypes *************************************************/
void ruby_start(const char *script);
int ruby_end(int retValue);
void ruby_run_plugins(const char *dir);
int isRubyScript(const char *name);

#endif
