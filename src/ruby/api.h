#ifndef RUBY_API_HEADER
#define RUBY_API_HEADER

/* Macros *********************************************************************/

/* Global Function Prototypes *************************************************/
void ruby_define_api();
VALUE ruby_get_api();
int ruby_api_fire(const char *event, const char *data);

#endif
