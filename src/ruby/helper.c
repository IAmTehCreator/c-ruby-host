#include <ruby.h>
#include <dirent.h>
#include <string.h>
#include "helper.h"

/* Private Function Prototypes ************************************************/
int string_ends_with(const char * str, const char * suffix);
void forFilesInDir(const char *dirName, void (*fn)(struct dirent *file));
void runScript(struct dirent *file);

/* Global Variables ***********************************************************/
char dirpath[255];

void ruby_start(const char *script) {
	ruby_init();
	ruby_script(script);
	ruby_init_loadpath();
}

int ruby_end(int retValue) {
	return ruby_cleanup(retValue);
}

void ruby_run_plugins(const char *dir) {
	strcpy(dirpath, dir);
	forFilesInDir(dir, runScript);
}

void forFilesInDir(const char *dirName, void (*fn)(struct dirent *file)) {
	DIR *directory = opendir(dirName);

	struct dirent *file;
	while((file = readdir(directory)) != NULL) {

		// If the directory entry is a regular file then call the callback
		if(file->d_type == DT_REG) {
			(fn)(file);
		}
	}

	closedir(directory);
}

void runScript(struct dirent *file) {
	const char *filename = file->d_name;

	char path[255];
	strcpy(path, dirpath);
	strcat(path, "/");

	if( isRubyScript(filename) ) {
		strcat(path, filename);

		rb_require(path);
	}
}

int isRubyScript(const char *name) {
	return string_ends_with(name, RUBY_SUFFIX);
}

int string_ends_with(const char * str, const char * suffix) {
	int str_len = strlen(str);
	int suffix_len = strlen(suffix);

	return (str_len >= suffix_len) && (0 == strcmp(str + (str_len-suffix_len), suffix));
}
