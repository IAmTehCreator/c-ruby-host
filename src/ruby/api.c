#include <ruby.h>
#include <stdio.h>
#include <utility/utility.h>
#include "api.h"

/* Private Function Prototypes ************************************************/
VALUE method_log(VALUE self, VALUE message);
VALUE method_on(VALUE self, VALUE event_name);
void all_iterator(const char *key, void *value);

static VALUE api;
static Map *listeners;

void ruby_define_api(const char *name, const char *version) {
	listeners = map_new();

	api = rb_define_module(name);

	rb_define_const(api, "VERSION", rb_sprintf("%s", version));

	rb_define_singleton_method(api, "log", method_log, 1);
	rb_define_singleton_method(api, "on", method_on, 1);
}

VALUE ruby_get_api() {
	return api;
}

int ruby_api_fire(const char *event, const char *data) {
	// TODO: Create an event class in Ruby and call the callback with an instance
	//       of the event class.

	VALUE *callback = map_get(listeners, event);
	rb_funcall(*callback, rb_intern("call"), 1, rb_sprintf("%s", data));

	return 0;
}

VALUE method_log(VALUE self, VALUE message) {
	puts(StringValueCStr(message));

	return Qnil;
}

VALUE method_on(VALUE self, VALUE event_name) {
  const char *event = StringValueCStr(event_name);
	rb_need_block();

  VALUE *callback = (VALUE *)malloc(sizeof(VALUE));
	*callback = rb_block_proc();

	map_put(listeners, event, callback);
	rb_gc_mark(*callback);

	return Qnil;
}
