################################################################################
# Test Module
################################################################################
# This module is to be used for C based unit tests.
################################################################################
module Test

	@on_event_result = nil;

	############################################################################
	# C Unit Test Helper Methods ###############################################
	############################################################################

	def Test.call_api_log message
		Api.log message
	end

	def Test.call_api_on event
		Api.on event do |i|
			@on_event_result = i
		end
	end

	def Test.call_api_on_exception event
		Api.on event do |i|
			raise 'Test Exception'
		end
	end

end

# Call verify method provided by the C host, this should only be called by one plugin
Test.verify
