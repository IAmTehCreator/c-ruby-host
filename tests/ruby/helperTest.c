#include <ruby.h>
#include <test/test.h>
#include "../src/ruby/helper.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
void before_each();
void after_each();
void test_constants();
void test_run_plugins();
void test_is_ruby_script();
VALUE method_ruby_verify(VALUE self);

/* Global Variables ***********************************************************/
static int verify_calls = 0;

int main(int argc, char *argv[]) {
	test_init("RubyHelper", argc, argv);
	ruby_start("Test");
	test_before_each(before_each);

	test("Constants", test_constants);
	test("ruby_run_plugins", test_run_plugins);
	test("isRubyScript", test_is_ruby_script);

	ruby_end(0);
	return test_complete();
}

void before_each() {
	verify_calls = 0;
}

void test_constants() {
	test_assert_string(RUBY_SUFFIX, ".rb", "Should set a constant for the Ruby script extension");
}

void test_run_plugins() {
	VALUE module = rb_define_module("Test");
	rb_define_singleton_method(module, "verify", method_ruby_verify, 0);

	// Assuming there is a Ruby script in the tests/plugin directory which whill call the
	// Test.verify method to show it's been loaded
	ruby_run_plugins("./tests/plugin");

	test_assert_int(verify_calls, 1, "Should require Ruby scripts from the './tests/plugin' directory");
}

void test_is_ruby_script() {
	test_assert( isRubyScript("script.rb"), "Should correctly assert that 'script.rb' is a Ruby script" );
	test_assert( isRubyScript("thing.c.rb"), "Should correctly assert that 'thing.c.rb' is a Ruby script" );

	test_assert( !isRubyScript("main.c"), "Should correctly assert that 'main.c' is not a Ruby script" );
	test_assert( !isRubyScript("file.rb.c"), "Should correctly assert that 'file.rb.c' is not a Ruby script" );
	test_assert( !isRubyScript("rb"), "Should correctly assert that 'rb' is not a Ruby script" );
}

VALUE method_ruby_verify(VALUE self) {
	verify_calls++;

	return Qnil;
}
