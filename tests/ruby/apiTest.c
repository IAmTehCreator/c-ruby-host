#include <ruby.h>
#include <test/test.h>
#include "../src/ruby/helper.h"
#include "../src/ruby/api.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
void test_ruby_define_api();
void test_ruby_api_fire();
void test_method_on_safe();
void ready_ruby();
void test_define_module();
VALUE method_verify(VALUE self);

/* Global Variables ***********************************************************/
static VALUE test_module;

int main(int argc, char *argv[]) {
	test_init("RubyApi", argc, argv);
	ready_ruby();

	test("ruby_define_api", test_ruby_define_api);
	test("ruby_api_fire", test_ruby_api_fire);
	xtest("API.on", test_method_on_safe);

	ruby_end(0);
	return test_complete();
}

void test_ruby_define_api() {
	VALUE version = rb_const_get_at( ruby_get_api(), rb_intern("VERSION"));

	test_assert_string( StringValueCStr(version), "0.0.0", "Should define a VERSION constant");
}

void test_ruby_api_fire() {
	rb_eval_string_protect("Test.call_api_on 'test'", 0);
	ruby_api_fire("test", "value");

	VALUE result = rb_iv_get(test_module, "@on_event_result");
	test_assert_string( StringValueCStr(result), "value", "Should pass the argument to the listener");
}

void test_method_on_safe() {
	rb_eval_string_protect("Test.call_api_on_exception 'test'", 0);
	ruby_api_fire("test", "value");
}

void ready_ruby() {
	ruby_start("Test");
	ruby_define_api("Api", "0.0.0");
	test_define_module();
	ruby_run_plugins("./tests/plugin");
}

void test_define_module() {
	test_module = rb_define_module("Test");
	rb_define_singleton_method(test_module, "verify", method_verify, 0);
}

VALUE method_verify(VALUE self) {
	return Qnil;
}
