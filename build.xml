<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="antlib:org.apache.tools.ant" xmlns:antlib="com.ant.lib" name="Ruby-Host" default="init">
	<description>A C application which hosts a Ruby environment.</description>

	<property file="local.build.properties" />
	<property file="build.properties" />

	<path id="classpath">
		<pathelement location="antlib.jar" />
	</path>

	<taskdef resource="antlib.xml" classpathref="classpath" uri="com.ant.lib" />

	<!-- ========================== Build Targets ========================== -->

	<target name="init" description="Initialises the build process" depends="deep.clean, build.libs" />

	<target name="clean" description="Deletes all build files">
		<delete dir="${bin.dir}" />
	</target>

	<target name="deep.clean" description="Deletes all build files and libraries" depends="clean">
		<antlib:for param="library">
			<dirset dir="${lib.dir}" includes="*" />
			<sequential>
				<antlib:propertyregex property="lib.name" input="@{library}" regexp=".*/(.*)" select="\1" override="true" />
				<delete dir="${lib.dir}/${lib.name}" />
			</sequential>
		</antlib:for>
	</target>

	<target name="build.libs" description="Extracts and compiles libraries">
		<antlib:for param="library">
			<fileset dir="${lib.dir}" includes="*.tar.gz" />
			<sequential>
				<antlib:propertyregex property="lib.name" input="@{library}" regexp=".*/(.*).tar.gz" select="\1" override="true" />

				<antlib:if><not><available file="${lib.dir}/${lib.name}/${lib.name}.a" /></not>
					<then>
						<echo>Extracting library ${lib.name}...</echo>
						<mkdir dir="${lib.dir}/${lib.name}" />
						<untar src="@{library}" dest="${lib.dir}/${lib.name}" compression="gzip" />
						<antlib:if><not><available file="${lib.dir}/${lib.name}/${lib.name}.a" /></not>
							<then>
								<echo>Compiling library ${lib.name}...</echo>
								<gcc from="${lib.dir}/${lib.name}" dir="${lib.dir}/${lib.name}" flags="-c" />
								<archive dir="${lib.dir}/${lib.name}" target="${lib.dir}/${lib.name}/${lib.name}.a" />
							</then>
						</antlib:if>
					</then>
				</antlib:if>
			</sequential>
		</antlib:for>
	</target>

	<target name="compile" description="Compiles the source code" depends="clean">
		<mkdir dir="${bin.dir}" />
		<gcc from="${bin.dir}" srcType="**/*.c" dir="${src.dir}" includes="${libs.all}" flags="-c" />
	</target>

	<target name="link" description="Links all of the compiled fragments into an executable binary" depends="compile">
		<gcc dir="${bin.dir}" srcType="*.o" libs="${lib.dir}" target="${bin.dir}/main" flags="-lruby" />
	</target>

	<target name="run" description="Runs the program" depends="link">
		<exec executable="${bin.dir}/main" />
	</target>

	<target name="test" description="Runs the unit tests for the C code" depends="compile">
		<mkdir dir="${bin.dir}/test" />
		<files like="*.o" not="main.o" in="${bin.dir}" var="src" />

		<antlib:for param="test">
			<fileset dir="${test.dir}" includes="**/*.c" />
			<sequential>
				<antlib:propertyregex property="src.file" input="@{test}" regexp=".*/(.*)Test.c" select="\1" override="true" />
				<antlib:if>
					<or>
						<and>
							<isset property="t" />
							<equals arg1="${t}" arg2="${src.file}" />
						</and>
						<not><isset property="t" /></not>
					</or>
					<then>
						<echo>${src.file}:</echo>

						<gcc source="@{test} ${src}"
							 libs="${lib.dir}"
							 target="${bin.dir}/test/${src.file}Test.o"
							 includes="${libs.all}"
							 flags="-lruby" />

						<exec executable="${bin.dir}/test/${src.file}Test.o" failonerror="true">
							<arg value="${test.args}" />
						</exec>

						<echo></echo>
					</then>
				</antlib:if>
			</sequential>
		</antlib:for>

		<delete>
			<fileset dir="${test.dir}" includes="*Test.bin" />
		</delete>
	</target>

	<!-- ============================== Macros ============================= -->

	<macrodef name="gcc" description="Invokes the GCC compiler with the provided arguments">
		<attribute name="source" default="" description="A space separated list of files to compile" />
		<attribute name="dir" default="" description="A directory where all '.c' files are to be compiled" />
		<attribute name="target" default="" description="Optional. The name of the executable to be created" />
		<attribute name="includes" default="" description="Optional. A ; separated list of libraries to include" />
		<attribute name="flags" default="" description="Optional. Flags to be provided to the compiler" />
		<attribute name="libs" default="" description="Optional. A directory containing .o libraries to include" />
		<attribute name="from" default="${basedir}" description="Optional. The directory to invoke GCC from" />
		<attribute name="srcType" default="*.c" description="Optional." />
		<sequential>
			<antlib:if>
				<and>
					<equals arg1="@{source}" arg2="" />
					<equals arg1="@{dir}" arg2="" />
				</and>
				<then>
					<fail message="macro 'gcc' requires at least the 'source' or 'dir' attributes to be specified" />
				</then>
			</antlib:if>

			<!-- Gets the source (.c) files to be fed into the compiler -->
			<antlib:var name="files" value="@{source}" />
			<antlib:if><equals arg1="${files}" arg2="" />
				<then>
					<files like="@{srcType}" in="@{dir}" var="files" />
				</then>
			</antlib:if>

			<antlib:var name="lib" value="" />
			<antlib:if><not><equals arg1="@{libs}" arg2="" /></not>
				<then>
					<files like="*.a" in="@{libs}" var="lib" />
				</then>
			</antlib:if>

			<!-- Prefix target with '-o' flag if it was provided -->
			<antlib:if><equals arg1="@{target}" arg2="" />
				<then>
					<antlib:var name="output" value="" />
				</then>
				<else>
					<antlib:var name="output" value="-o @{target}" />
				</else>
			</antlib:if>

			<!-- Prefix each library path with the -I flag -->
			<antlib:var name="include" value="" />
			<antlib:if><not><equals arg1="@{includes}" arg2="" /></not>
				<then>
					<antlib:for list="@{includes}" delimiter=";" param="lib">
						<sequential>
							<antlib:var name="include" value="${include} -I@{lib}" />
						</sequential>
					</antlib:for>
				</then>
			</antlib:if>

			<exec executable="/bin/bash" dir="@{from}">
				<arg value="-c" />
				<arg value="gcc ${include} @{flags} ${output} ${files} ${lib}" />
			</exec>
		</sequential>
	</macrodef>

	<macrodef name="archive" description="Creates a library archive containing multiple object files">
		<attribute name="dir" description="The directory to read the object files from" />
		<attribute name="target" description="The archive file to create" />
		<sequential>
			<files like="*.o" in="@{dir}" var="objects" />

			<exec executable="/bin/bash" dir="${basedir}">
				<arg value="-c" />
				<arg value="ar rcs @{target} ${objects}" />
			</exec>
		</sequential>
	</macrodef>

	<macrodef name="files" description="Iterates over files of a specified type in a directory and returns a list of them">
		<attribute name="like" description="The filename selector for the files including wildcards" />
		<attribute name="not" default="" description="Optional. A filename selector with wildcards of files to be excluded from the list" />
		<attribute name="in" description="The directory to search for files within" />
		<attribute name="var" description="The name of the variable to set" />
		<sequential>
			<antlib:var name="@{var}" value="${files.list}" />
			<antlib:var name="files.list" value="" />
			<antlib:for param="object.file">
				<path>
					<fileset dir="@{in}">
						<include name="**/@{like}" />
						<exclude name="@{not}" />
					</fileset>
				</path>
				<sequential>
					<antlib:var name="files.list" value="${files.list} @{object.file}" />
				</sequential>
			</antlib:for>
			<antlib:var name="@{var}" value="${files.list}" />
		</sequential>
	</macrodef>

</project>
